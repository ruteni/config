const resolve = require("@rollup/plugin-node-resolve").nodeResolve;
const component = require("rollup-plugin-component").default;
const commonjs = require("@rollup/plugin-commonjs");
const { terser } = require("rollup-plugin-terser");
const replace = require("@rollup/plugin-replace");
const json = require("@rollup/plugin-json");
const copy = require("rollup-plugin-copy");
const html = require("rollup-plugin-html");
const fs = require("fs-extra");
const path = require("path");

const isProduction = process.env.BUILD === "production";

const replacement = {
    "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV || "production"),
    preventAssignment: true
};

const htmlMinifierOptions = {
    collapseBooleanAttributes: true,
    collapseWhitespace: true,
    removeComments: true,
    removeOptionalTags: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: true,
    removeTagWhitespace: true
};

class RuteniConfig {
    constructor(opts = {}) {
        this.component = component.get();
        this.componentName = opts.componentName || this.component.name;
        this.resourcesDir = opts.resourcesDir || "../resources";
        this.tmpDir = opts.tmpDir || "tmp";
        this.imagesDir = opts.imagesDir || "images";
        this.elementsDir = opts.elementsDir || "elements";
        this.installDir = opts.installDir || path.join(component.gitRoot(), "dist");
        this.manifestsDir = opts.manifestsDir || path.join(this.installDir, "manifests");
        this.sysManifestsDir = opts.sysManifestsDir || "/var/lib/totates/manifests";
        this.staticDir = opts.staticDir || path.join(this.installDir, "static");
        this.outputDir = opts.outputDir || path.join(this.staticDir, this.componentName);
        this.manifestsPath = opts.manifestsPath || `${this.manifestsDir}:${this.sysManifestsDir}`;
        this.app = opts.app || this.component.app;
        this.webManifest = opts.webManifest;
        this.paths = component.resolve(this.manifestsPath);

        this.rollupConfig = [];

        this.componentConfig = {
            installDir: this.installDir,
            resourcesDir: this.resourcesDir,
            manifestsPath: this.manifestsPath,
            select: component.select([
                `${this.componentName}:image.*`,
                `${this.componentName}:element.*.fluent`,
                `${this.componentName}:element.*.style`,
                `${this.componentName}:element.*.index`,
                "@ruteni/quotquot/v1:element",
                "@ruteni/quotquot/v1:espression",
                "@ruteni/quotquot/v1:router",
                "@ruteni/quotquot/v1:plugin.*"
            ]),
            resources: [
                {
                    resource: "scan",
                    recursive: true,
                    prefix: "image",
                    suffix: ".png",
                    type: "fetch",
                    srcDir: this.imagesDir,
                    destDir: this.imagesDir,
                    processNames: (assetName, filepath, newrelpath) => [
                        assetName.replace(/^icons\./u, ""), newrelpath
                    ]
                }
            ]
        };
        this.addElements(this.elementsDir);
        if (this.app) {
            this.addApp(
                this.app.swSrc,
                this.app.webManifest,
                this.app.indexHtml,
                this.app.routesJson
            );
        }
        this.addMain();
    }

    addApp(swSrc, webManifest, indexHtml, routesJson) {
        if (!swSrc || !webManifest || !indexHtml)
            throw new Error("missing swSrc, webManifest, or indexHtml");

        // swSrc
        this.rollupConfig.push({
            input: swSrc,
            output: {
                file: path.join(this.tmpDir, swSrc),
                format: "esm",
                sourcemap: !isProduction
            },
            plugins: [
                replace(replacement),
                resolve({ browser: true }),
                isProduction && terser()
            ]
        });

        // indexHtml
        const indexHtmlContent = component.processHtml(fs.readFileSync(indexHtml));
        const additionalManifestEntries = [
            { url: ".", revision: component.computeRevision(indexHtmlContent, "md5") }
        ];
        fs.writeFileSync(path.join(this.resourcesDir, "index.html"), indexHtmlContent);

        // routesJson
        if (routesJson) { // TODO: not a standard app feature; move elsewhere
            const routesJsonContent = JSON.stringify(fs.readJsonSync(routesJson));
            additionalManifestEntries.push({
                url: "routes.json",
                revision: component.computeRevision(routesJsonContent, "md5")
            });
            fs.writeFileSync(
                path.join(this.resourcesDir, "routes.json"), routesJsonContent
            );
        }

        // webManifest
        this.componentConfig.workbox = {};
        for (const lang of Object.keys(webManifest.lang)) {
            // TODO: create plugin for manifest creation
            fs.ensureDirSync(path.join(this.resourcesDir, lang));
            fs.writeJsonSync(
                path.join(this.resourcesDir, lang, "manifest.json"),
                Object.assign({}, webManifest, { lang }, webManifest.lang[lang])
            );
            // localizedResources[lang] = [ path.join(lang, "manifest.json") ];
            this.componentConfig.workbox[lang] = {
                swSrc: path.join(this.tmpDir, swSrc),
                swDest: path.join(this.resourcesDir, lang, swSrc),
                globDirectory: "dist",
                // globPatterns: ["*.html", "*.json"],
                // modifyURLPrefix: { "": this.app.baseUrl },
                additionalManifestEntries
            };
        }
    }

    addMain() {
        this.rollupConfig.push({
            input: "index.js",
            output: {
                file: path.join(this.outputDir, "index.js"),
                format: "esm",
                sourcemap: !isProduction,
                paths: this.paths
            },
            external: id => component.external(id) || id.startsWith("/static"),
            plugins: [
                component(this.componentConfig),
                isProduction && terser()
            ]
        });
    }

    addElement(elementsDir, elementName) {
        const elementDir = path.join(elementsDir, elementName);
        const destElementDir = path.join(this.tmpDir, elementsDir, elementName);
        this.rollupConfig.push({
            input: path.join(elementDir, "index.js"),
            output: {
                file: path.join(destElementDir, "index.js"),
                format: "esm",
                paths: this.paths
            },
            external: component.external(),
            plugins: [
                json(),
                commonjs(),
                resolve({ browser: true }),
                component.resources(
                    this.manifestsPath,
                    path.join(elementDir, "index.json"),
                    path.join(destElementDir, "resources.js")
                ),
                html({
                    include: path.join(elementDir, "index.html"),
                    htmlMinifierOptions
                }),
                copy({
                    targets: [
                        {
                            src: path.join(elementDir, "index.css"),
                            dest: destElementDir
                        },
                        {
                            src: path.join(elementDir, "locales/**/*"),
                            dest: path.join(destElementDir, "locales")
                        }
                    ]
                }),
                process.env.BUILD === "production" && terser()
            ]
        });
    }

    addElements(elementsDir) {
        for (const name of fs.readdirSync(elementsDir))
            this.addElement(elementsDir, name);
        this.componentConfig.resources.push(
            {
                resource: "scan",
                suffix: ".ftl",
                type: "fetch",
                recursive: true,
                processNames: (assetName, filepath, relPath) => [
                    assetName.replace(
                        /(.*)\.locales\.(.*)/u, "element.$1.fluent[lang=$2]"
                    ),
                    relPath
                ],
                srcDir: path.join(this.tmpDir, elementsDir),
                destDir: elementsDir
            },
            {
                resource: "scan",
                suffix: ".css",
                type: "style",
                recursive: true,
                processNames: (assetName, filepath, relPath) => [
                    assetName.replace(/(.*)\.index/u, "element.$1.style"), relPath
                ],
                srcDir: path.join(this.tmpDir, elementsDir),
                processContent: content => component.processCss(content.toString()),
                destDir: elementsDir
            },
            {
                resource: "scan",
                suffix: ".js",
                type: "script",
                recursive: true,
                processNames: (assetName, filepath, relPath) => [
                    assetName.replace(
                        /(.*)\.(index|resources)/u,
                        "element.$1.$2"
                    ), relPath
                ],
                srcDir: path.join(this.tmpDir, elementsDir),
                destDir: elementsDir
            }
        );
    }

    getRollup() {
        return this.rollupConfig;
    }
}

module.exports = RuteniConfig;
